#! /usr/bin/env bash

MY_NAME="Run Steam"
MY_PATH=${PWD%${MY_NAME}.app/Contents/Resources}

function p()
{
    echo -e "${1}"
}

function info()
{
    p "*** Current directory:\n\t${PWD}"
}


function stop_ipcserver()
{
    p "*** Stopping Steam's ipcserver program..."
    pkill ipcserver
    p "*** Checking if Steam's ipcserver program has stopped..."
    pgrep -ql ipcserver
    [[ ${?} -eq 0 ]] && { p "Steam's ipcserver is still running. Please try to stop it manually."; }
    p "*** Done."
}

function cleanup()
{
    stop_ipcserver
}

function start_ipcserver()
{
    p "*** Starting Steam's ipcserver program..."
    "${MY_PATH}/Library/ApplicationSupport/Steam/Steam.AppBundle/Steam/Contents/MacOS/ipcserver" &
    p "*** Checking if Steam's ipcserver program is running..."
    pgrep -l ipcserver
    [[ ${?} -ne 0 ]] && { echo "Steam's ipcserver process cannot be started. This means that games will not work. Exiting."; cleanup; exit -1; }
    p "*** Done."
}

function init()
{
    start_ipcserver
}

trap ' { cleanup; }; ' ABRT EXIT HUP INT TERM QUIT

info
init
open -W "${MY_PATH}/Steam.app"
wait
cleanup
